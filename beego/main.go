package main

import (
	_ "beego/www/routers"
	"github.com/astaxie/beego"
)

func main() {
	beego.Run()
}
